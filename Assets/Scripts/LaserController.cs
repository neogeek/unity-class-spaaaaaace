﻿using UnityEngine;

public class LaserController : MonoBehaviour {

    public GameObject explosion;

    public GameObject shotBy;

    private Rigidbody2D rb;
    private Collider2D collider;

    private readonly float speed = 500f;

    void Awake() {

        rb = gameObject.GetComponent<Rigidbody2D>();

    }

    void Start() {

        rb.AddForce(transform.up * speed);

        Physics2D.IgnoreCollision(shotBy.GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());

    }

    void OnCollisionEnter2D(Collision2D other) {

        if (other.gameObject.tag.Equals("Shield")) {

            Destroy(gameObject);

            return;

        } else if (other.gameObject.tag.Equals("Enemy")) {

            other.gameObject.GetComponent<EnemyController>().Hit(10);

        } else if (other.gameObject.tag.Equals("Player")) {

            other.gameObject.GetComponent<PlayerController>().Hit(10);

        }

        Destroy(gameObject);

        Instantiate(explosion, gameObject.transform.position, Quaternion.identity);

        Camera.main.gameObject.GetComponent<ScottDoxey.ScreenShake>().Shake();

    }

}
