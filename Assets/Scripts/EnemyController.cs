﻿using System.Collections;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public GameObject explosion;

    public GameObject laserProjectile;
    public GameObject[] laserSpawnPoints;
    public LayerMask playerLayerMask;

    public bool followPlayer = false;

    private GameObject player;

    private Rigidbody2D rb;

    private bool isFiring = false;

    private int health = 100;

    private readonly float thrust = 1f;

    void Awake() {

        player = GameObject.Find("Player");

        rb = gameObject.GetComponent<Rigidbody2D>();

        StartCoroutine("Fire");

    }

    void Update() {

        if (player) {

            rb.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2((player.transform.position.y - transform.position.y), (player.transform.position.x - transform.position.x)) * Mathf.Rad2Deg - 90);

        }

    }

    void FixedUpdate() {

        if (followPlayer && player) {

            gameObject.transform.position = Vector2.Lerp(gameObject.transform.position, player.transform.position, thrust * Time.deltaTime);

        }

    }

    public void Hit(int hitPoints) {

        health -= hitPoints;

        if (health < 0) {

            Instantiate(explosion, gameObject.transform.position, Quaternion.identity);

            Destroy(gameObject);

        }

    }

    IEnumerator Fire() {

        if (!isFiring) {

            isFiring = true;

            yield return new WaitForSeconds(Random.Range(0.2f, 2.0f));

            GameObject spawnedProjectile = Instantiate(laserProjectile, gameObject.transform.position, gameObject.transform.rotation);

            spawnedProjectile.GetComponent<LaserController>().shotBy = gameObject;

            isFiring = false;

            StartCoroutine("Fire");

        }

    }

}
