﻿using UnityEngine;

public class LaserRayController : MonoBehaviour {

    public bool isFiring = false;

    public LineRenderer lineRenderer;

    public GameObject explosionShip;
    public GameObject explosionGeneric;

    public LayerMask enemyLayerMask;

    void FixedUpdate() {

        if (isFiring) {

            RaycastHit2D hit = Physics2D.Raycast(gameObject.transform.position, gameObject.transform.up, Mathf.Infinity, enemyLayerMask);

            Debug.DrawRay(gameObject.transform.position, gameObject.transform.up * 10f, Color.red);

            if (hit) {

                lineRenderer.enabled = true;

                lineRenderer.SetPosition(0, gameObject.transform.position);
                lineRenderer.SetPosition(1, hit.point);

                GameObject hitObj = hit.collider.gameObject;

                if (hitObj.tag.Equals("Enemy")) {

                    hitObj.GetComponent<EnemyController>().Hit(10);

                    Instantiate(explosionShip, hitObj.transform.position, Quaternion.identity);

                } else if (hitObj.tag.Equals("Player")) {

                    hitObj.GetComponent<PlayerController>().Hit(10);

                    Instantiate(explosionShip, hitObj.transform.position, Quaternion.identity);

                } else {

                    Instantiate(explosionGeneric, hit.point, Quaternion.identity);

                }

            } else {

                lineRenderer.enabled = false;

            }

        } else {

            lineRenderer.enabled = false;

        }

    }

}
