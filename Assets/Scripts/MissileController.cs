﻿using UnityEngine;

public class MissileController : MonoBehaviour {

    public LayerMask layerMask;
    public GameObject explosion;

    public GameObject shotBy;

    private Collider2D collider;

    private readonly float explosionRadius = 2f;

    void Start() {

        if (shotBy) {

            Physics2D.IgnoreCollision(shotBy.GetComponent<Collider2D>(), gameObject.GetComponent<Collider2D>());

        }

    }

    void OnCollisionEnter2D(Collision2D other) {

        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(gameObject.transform.position, explosionRadius, layerMask);

        foreach (Collider2D hitCollider in hitColliders) {

            if (hitCollider.gameObject.tag.Equals("Enemy") || hitCollider.gameObject.tag.Equals("Player")) {

                if (hitCollider.gameObject.tag.Equals("Enemy")) {

                    hitCollider.gameObject.GetComponent<EnemyController>().Hit(50);

                } else if (hitCollider.gameObject.tag.Equals("Player")) {

                    hitCollider.gameObject.GetComponent<PlayerController>().Hit(50);

                }

            }

        }

        Destroy(gameObject);

        Instantiate(explosion, gameObject.transform.position, Quaternion.identity);

    }

}
