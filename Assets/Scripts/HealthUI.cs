﻿using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour {

    public PlayerController playerController;

    public Sprite[] lifeBarImages;
    public Image lifeBar;

    void Update() {

        lifeBar.sprite = lifeBarImages[(int) Mathf.Lerp(0, lifeBarImages.Length - 1, Mathf.InverseLerp(0, playerController.maxHealth, playerController.health))];

    }

}
