﻿using System.Collections;
using UnityEngine;

public class ShieldController : MonoBehaviour {

    public bool isShieldCharged = true;
    public bool isShieldActive = false;

    public Renderer renderer;
    public Collider2D collider;

    void Awake() {

        renderer = gameObject.GetComponent<Renderer>();
        collider = gameObject.GetComponent<Collider2D>();

    }

    void Update() {

        renderer.enabled = isShieldActive;
        collider.enabled = isShieldActive;

    }

    public void ActivateShield() {

        if (isShieldCharged && !isShieldActive) {

            StartCoroutine("DischargeShield");

        }

    }

    IEnumerator DischargeShield() {

        isShieldActive = true;

        yield return new WaitForSeconds(5f);

        isShieldActive = false;
        isShieldCharged = false;

        StartCoroutine("ChargeShield");

    }

    IEnumerator ChargeShield() {

        yield return new WaitForSeconds(5f);

        isShieldCharged = true;

    }

}
