﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public ShieldController shieldController;

    public GameObject explosion;

    public GameObject laserProjectile;
    public GameObject missileProjectile;
    public GameObject[] laserSpawnPoints;
    public GameObject[] laserRaySpawnPoints;
    public GameObject[] missileSpawnPoints;
    public LayerMask enemyLayerMask;

    private Vector2 mousePosition;
    private Rigidbody2D rb;

    private bool isFiring = false;
    private bool isFiringMissile = false;

    private LaserRayController laserRayController;

    public float health = 100;
    public readonly float maxHealth = 100;

    private readonly WaitForSeconds delayBetweenShots = new WaitForSeconds(0.05f);
    private readonly WaitForSeconds delayBetweenMissileShots = new WaitForSeconds(1f);
    private readonly float thrust = 50f;

    void Awake() {

        rb = gameObject.GetComponent<Rigidbody2D>();

        laserRayController = gameObject.GetComponent<LaserRayController>();

    }

    void Update() {

        mousePosition = Camera.main.ScreenToWorldPoint((Vector2) Input.mousePosition);

        rb.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2((mousePosition.y - transform.position.y), (mousePosition.x - transform.position.x)) * Mathf.Rad2Deg - 90);

        if (Input.GetKey("space")) {

            rb.AddForce(transform.up * thrust);

        }

        if (Input.GetKey("q")) {

            shieldController.ActivateShield();

        }

        if (Input.GetMouseButtonDown(0)) {

            isFiring = true;

            StartCoroutine("FireLaser");

        } else if (Input.GetMouseButtonUp(0)) {

            isFiring = false;

        } else if (Input.GetMouseButtonDown(1)) {

            laserRayController.isFiring = true;

        } else if (Input.GetMouseButtonUp(1)) {

            laserRayController.isFiring = false;

        } else if (Input.GetKeyDown("m")) {

            isFiringMissile = true;

            StartCoroutine("FireMissile");

        } else if (Input.GetKeyUp("m")) {

            isFiringMissile = false;

        }

    }

    public void Hit(int hitPoints) {

        health -= hitPoints;

        if (health < 0) {

            Instantiate(explosion, gameObject.transform.position, Quaternion.identity);

            Destroy(gameObject);

        }

    }

    IEnumerator FireLaser() {

        if (isFiring) {

            foreach (GameObject projectileSpawnPoint in laserSpawnPoints) {

                GameObject spawnedProjectile = Instantiate(laserProjectile, projectileSpawnPoint.transform.position, projectileSpawnPoint.transform.rotation);

                spawnedProjectile.GetComponent<LaserController>().shotBy = gameObject;

            }

            yield return delayBetweenShots;

            StartCoroutine("FireLaser");

        }

    }

    IEnumerator FireMissile() {

        if (isFiringMissile) {

            foreach (GameObject projectileSpawnPoint in missileSpawnPoints) {

                GameObject spawnedProjectile = Instantiate(missileProjectile, projectileSpawnPoint.transform.position, projectileSpawnPoint.transform.rotation);

                spawnedProjectile.GetComponent<MissileController>().shotBy = gameObject;

            }

            yield return delayBetweenMissileShots;

            StartCoroutine("FireMissile");

        }

    }

}
