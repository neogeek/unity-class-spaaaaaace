﻿using UnityEngine;
using UnityEngine.UI;

public class ShieldUI : MonoBehaviour {

    public ShieldController shieldController;

    public Sprite shieldCharged;
    public Sprite shieldDisharged;

    public Image shieldImage;

    void Update() {

        if (shieldController.isShieldCharged) {

            shieldImage.sprite = shieldCharged;

        } else {

            shieldImage.sprite = shieldDisharged;

        }

    }

}
